package bean;


public class DeleteBean {
    String roomnumber;
    String roomtype;
    String beds;
    String price;
	 
	public DeleteBean(){
	}
	
	public String getRoomnumber() {
		return roomnumber;
	}
	public void setRoomnumber(String roomnumber) {
		this.roomnumber = roomnumber;
	}
	public String getRoomtype() {
		return roomtype;
	}
	public void setRoomtype(String roomtype) {
		this.roomtype = roomtype;
	}
	public String getBeds() {
		return beds;
	}
	public void setBeds(String beds) {
		this.beds=beds;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price=price;
	}
	
}



