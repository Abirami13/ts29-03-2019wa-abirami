package bean;

public class UpdateBean {
	String hostelname;
    String roomnumber;
    String roomtype;
    String beds;
    String price;
    public UpdateBean(){
	}
	
    public String getHostelname() {
		return hostelname;
	}

	public void setHostelname(String hostelname) {
		this.hostelname = hostelname;
	}
	
	 
	
	public String getRoomnumber() {
		return roomnumber;
	}
	public void setRoomnumber(String roomnumber) {
		this.roomnumber = roomnumber;
	}
	public String getRoomtype() {
		return roomtype;
	}
	public void setRoomtype(String roomtype) {
		this.roomtype = roomtype;
	}
	public String getBeds() {
		return beds;
	}
	public void setBeds(String beds) {
		this.beds=beds;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price=price;
	}
	
}




