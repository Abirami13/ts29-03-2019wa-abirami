package controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import bean.ImageBean;

import java.sql.Blob;

import dao.ImageDAO;
@MultipartConfig(maxFileSize = 20000000)
public class ImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("multipart/form-data");
		String hostelid=request.getParameter("hostelid");
	
		String specifications=request.getParameter("specifications");
		String beds=request.getParameter("choose");
		 String price=request.getParameter("Price");
		 String checkin=request.getParameter("checkin");
		 String checkout=request.getParameter("checkout");
		 String guests=request.getParameter("guest");
		 String rooms=request.getParameter("rooms");
		 
		
			
					 
		 System.out.println("hostel from servlet"+checkin);
		 ImageBean insert=new ImageBean();
		 insert.setHostelid(hostelid);
		 insert.setSpecifications(specifications);
		 insert.setBeds(beds);
		 insert.setPrice(price);
		 insert.setCheckin(checkin);
		 insert.setCheckout(checkout);
		 insert.setGuests(guests);
		 insert.setRooms(rooms);
		 
	        
		 System.out.println("date"+checkin);
		 System.out.println("date"+checkout);

		  
		 ImageDAO insertDao = new ImageDAO(); 
		 insertDao.InsertData(insert);
		 
		 InputStream inputStream = null;
			Part imagePart = request.getPart("image");
			if (imagePart != null) {
	        inputStream = imagePart.getInputStream();
			}
			
	        ImageDAO PicInsert = new ImageDAO();
			PicInsert.insertImage(inputStream,hostelid);
			
		

		 response.setContentType("text/html");
		 PrintWriter writer =  response.getWriter();
		writer.println("<h3 style=\"color:Tomato;\">inserted </h3>");	           
		 writer.close();
	  
	}

}



