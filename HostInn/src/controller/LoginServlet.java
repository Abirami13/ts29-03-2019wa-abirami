package controller;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.LoginBean;
import dao.LoginDAO;


public class LoginServlet extends HttpServlet {

public LoginServlet() {
}

protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


String userName = request.getParameter("username");
String password = request.getParameter("password");
System.out.println("search------>"+userName);

LoginBean loginBean = new LoginBean();

loginBean.setUserName(userName); 
loginBean.setPassword(password);

LoginDAO loginDao = new LoginDAO();

String userValidate = loginDao.authenticateUser(loginBean); 

if(userValidate.equals("SUCCESS")) 
{
request.setAttribute("userName", userName); 
request.getRequestDispatcher("/hosteldetails.jsp").forward(request, response);
}
else
{
request.setAttribute("errMessage", userValidate);
request.getRequestDispatcher("/index.jsp").forward(request, response);
}
}
}

