package controller;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.AdminBean;
import bean.InsertBean;
import bean.LoginBean;
import dao.AdminDAO;
import dao.InsertDAO;
import dao.LoginDAO;
 
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String hostelname=request.getParameter("hostelname");
		String roomnumber=request.getParameter("ROOM NUMBER");
		 String roomtype=request.getParameter("ROOM TYPE");
		 String beds=request.getParameter("BEDS");
		 String price=request.getParameter("PRICE");
		 
		 System.out.println("roomnumber from servlet"+roomnumber);
		 AdminBean insert=new AdminBean();
		 insert.setHostelname(hostelname);
		 insert.setRoomnumber(roomnumber);
		 insert.setRoomtype(roomtype);
		 insert.setBeds(beds);
		 insert.setPrice(price);
		 
		  
		 AdminDAO insertDao = new AdminDAO(); 
		 insertDao.InsertData(insert);
		 
		 response.setContentType("text/html");
		 request.getRequestDispatcher("/viewrooms.jsp").forward(request, response);
	//	 PrintWriter writer =  response.getWriter();
	//	writer.println("<h1 style=\"color:Tomato;\"> Inserted Succesfully </h1>");	           
	//	 writer.close();
	  
	}

}
