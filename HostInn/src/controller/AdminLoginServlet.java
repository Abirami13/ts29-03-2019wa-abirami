package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.AdminLoginBean;
import bean.LoginBean;
import dao.AdminLoginDAO;
import dao.LoginDAO;


public class AdminLoginServlet extends HttpServlet {

public AdminLoginServlet() {
}

protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


String userName = request.getParameter("username");
String password = request.getParameter("password");
System.out.println("search------>"+userName);

AdminLoginBean adminloginBean = new AdminLoginBean();

adminloginBean.setUserName(userName); 
adminloginBean.setPassword(password);

AdminLoginDAO loginDao = new AdminLoginDAO();

String userValidate = loginDao.authenticateUser(adminloginBean); 

if(userValidate.equals("SUCCESS")) 
{
request.setAttribute("userName", userName); 
request.getRequestDispatcher("/adminpage.jsp").forward(request, response);
}
else
{
request.setAttribute("errMessage", userValidate);
request.getRequestDispatcher("/index.jsp").forward(request, response);
}
}
}


