package controller;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.DeleteBean;
import bean.InsertBean;
import dao.DeleteDAO;
import dao.InsertDAO;



public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String roomnumber=request.getParameter("ROOM NUMBER");
		 String roomtype=request.getParameter("ROOM TYPE");
		 String beds=request.getParameter("BEDS");
		 String price=request.getParameter("PRICE");
		 System.out.println("roomnumber from servlet"+roomnumber);
		 
		
		 

		 DeleteBean delete=new DeleteBean();
		 
		 delete.setRoomnumber(roomnumber);
		 delete.setRoomtype(roomtype);
		 delete.setBeds(beds);
		 delete.setPrice(price);
		  
		
		  
		DeleteDAO deleteDao = new DeleteDAO(); 
		 deleteDao.delete(delete);
		 
		 response.setContentType("text/html");
//		 PrintWriter writer =  response.getWriter();
	//	 writer.println("<h1 style=\"color:blue;\"> deleted Succesfully </h1>");	           
		// writer.close();
		 request.getRequestDispatcher("/viewrooms.jsp").forward(request, response);

	}
}
