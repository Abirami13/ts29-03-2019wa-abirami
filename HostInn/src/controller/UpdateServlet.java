package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.InsertBean;
import bean.UpdateBean;
import dao.InsertDAO;
import dao.UpdateDAO;


public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String hostelname=request.getParameter("hostelname");
		String roomnumber=request.getParameter("ROOM NUMBER");
		 String roomtype=request.getParameter("ROOM TYPE");
		 String beds=request.getParameter("BEDS");
		 String price=request.getParameter("PRICE");
		 System.out.println("roomnumber from servlet"+roomnumber);
		 
		 UpdateBean update=new UpdateBean();
		 update.setHostelname(hostelname);
		 update.setRoomnumber(roomnumber); 
		 update.setRoomtype(roomtype);
		 update.setBeds(beds);
		 update.setPrice(price);
		  
		 UpdateDAO updateDao = new UpdateDAO(); 
		 updateDao.update(update);
		 
		 response.setContentType("text/html");
		 request.getRequestDispatcher("/viewrooms.jsp").forward(request, response);
//		 PrintWriter writer =  response.getWriter();
//		 writer.println("<h1 style=\"color:blue;\"> Updated Succesfully </h1>");	           
//		 writer.close();
	}
}

