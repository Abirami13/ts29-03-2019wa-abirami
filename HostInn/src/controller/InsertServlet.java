package controller;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.InsertBean;
import bean.LoginBean;
import dao.InsertDAO;
import dao.LoginDAO;
 
public class InsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String username=request.getParameter("Name");
		 String password=request.getParameter("psw");
		 String email=request.getParameter("EMail");
		 String phonenumber=request.getParameter("Phonenumber");
		 
		 
		 
		 System.out.println("username from servlet"+username);
		 InsertBean insert=new InsertBean();
		 
		 insert.setUsername(username); 
		 insert.setPassword(password);
		 insert.setEmailaddress(email); 
		 insert.setPhonenumber(phonenumber);
		 
		  
		 InsertDAO insertDao = new InsertDAO(); 
		 insertDao.InsertData(insert);
		 
		 response.setContentType("text/html");
		 request.getRequestDispatcher("/hosteldetails.jsp").forward(request, response);
		// PrintWriter writer =  response.getWriter();
		 //writer.println("<h1 style=\"color:Tomato;\"> Registered Succesfully </h1>");	           
		 //writer.close();
	  
	}

}

