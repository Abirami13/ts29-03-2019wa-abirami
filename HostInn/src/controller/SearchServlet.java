package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.SearchBean;
import dao.SearchDAO;

public class SearchServlet extends HttpServlet {

public SearchServlet() {
}

protected void doGet(HttpServletRequest request, HttpServletResponse response) 
		throws ServletException, IOException {
	String  name = request.getParameter("search");
 
	SearchBean searchBean = new SearchBean();

	 searchBean.setWords(name);


	SearchDAO searchDao = new SearchDAO();
	String search=searchDao.authenticateUser(searchBean);
	System.out.println("search------>"+search);
	
	response.setContentType("text/html");
	PrintWriter out=response.getWriter();
 
	
	
	out.println("search result--->"+search);
	out.close();

}
}
