package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;

import bean.UpdateBean;
import connection.DBConnection;
 

public class UpdateDAO {

  public void update(UpdateBean updatebean) {
	  String hostelname=updatebean.getHostelname();
	  String roomnumber=updatebean.getRoomnumber();
	  String roomtype=updatebean.getRoomtype();
	  String beds=updatebean.getBeds();
	  String price=updatebean.getPrice();
	  
	  Connection con = null;
	  System.out.println("DaO"+roomnumber);
	  int s=Integer.parseInt(roomnumber);

	  try {
			con = DBConnection.createConnection();
			PreparedStatement updateStatement = con.prepareStatement("Update adminentry set hostelname =(?),roomnumber=(?),roomtype = (?), beds = (?), price=(?) where roomnumber = (?)");
			updateStatement.setString(1, hostelname);
			updateStatement.setInt(2, s);
			updateStatement.setString(3, roomtype);
			updateStatement.setString(4, beds);
			updateStatement.setString(5, price);
			updateStatement.setInt(6, s);
			
			
			
			
			int i = updateStatement.executeUpdate();
			System.out.println(i+ "row updated");
		}
		catch(Exception updateError) {
			System.out.println(updateError);
		}
	}  
  }

 

