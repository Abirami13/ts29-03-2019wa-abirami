package dao;
import java.sql.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import bean.InsertBean;
import connection.DBConnection;
public class InsertDAO {
	public String InsertData(InsertBean insert) {
		String username=insert.getUsername();
		String password=insert.getPassword();
		String emailaddress=insert.getEmailaddress();
		String phonenumber=insert.getPhonenumber();
		
		System.out.println("------------->"+username);
		Connection con = null;
	 
	 
		 
		try
		 {
		 con = DBConnection.createConnection(); 
		 PreparedStatement stmt=con.prepareStatement("insert into register values(?,?,?,?)");
		 stmt.setString(1, username);
		 stmt.setString(2,password);
		 stmt.setString(3,emailaddress);
		 stmt.setString(4,phonenumber);
		 
		 	
		 int i=stmt.executeUpdate();  
			System.out.println(i+" record inserted");  
			  
			con.close();  
		 }
		 catch(SQLException e)
		 {
		 e.printStackTrace();
		 }
		 return "Invalid user credentials"; 
	}

}


