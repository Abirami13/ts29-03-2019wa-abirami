package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;

import bean.DeleteBean;
import connection.DBConnection;
 

public class DeleteDAO {

  public void delete(DeleteBean deletebean) {
	  
	    String roomnumber=deletebean.getRoomnumber();
		String roomtype =deletebean.getRoomtype();
		String beds=deletebean.getBeds();
		String price=deletebean.getPrice();
		
		
	  
	  Connection con = null;
	  System.out.println("DaO"+roomnumber);
	  int s=Integer.parseInt(roomnumber);
	  try {
		  con = DBConnection.createConnection();
			PreparedStatement deleteStatement = con.prepareStatement("delete from adminentry where roomnumber=(?)");
		  
			deleteStatement.setInt(1, s);  
			int i = deleteStatement.executeUpdate();
			System.out.println(i+ "row deleted");
		}
		catch(Exception updateError) {
			System.out.println(updateError);
		} 
	}  
  }

 
