package dao;

import java.io.InputStream;
import java.sql.*;

import java.sql.Blob;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import bean.ImageBean;
import connection.DBConnection;
import controller.date;
public class ImageDAO {
	public String InsertData(ImageBean insert) {
		String hostelid=insert.getHostelid();

		String specifications=insert.getSpecifications();
		String beds =insert.getBeds();
		String price=insert.getPrice();
		String checkin=insert.getCheckin();
		String checkout=insert.getCheckout();
		String guests=insert.getGuests();
		String rooms=insert.getRooms();
		
		
		System.out.println("------------->"+beds );
		Connection con = null;
		
		System.out.println(checkin);
		System.out.println(checkout);
		
		 String[] arrOfStr = checkin.split("-",3);
			
		 ArrayList <Integer> b=new    ArrayList<Integer>();
		 
		 for (String a : arrOfStr) { 
			  int number=Integer.parseInt(a);
			  b.add(number);
	    
		  } 
		 
	 Iterator df=b.iterator();
	 
	 String[] gh=new String[3];
	 int j=0;
	 while(df.hasNext()) {
		 gh[j]= ""+df.next();
		 j++;
		 
	 }
	 
	 
	 
	
	String fv=gh[2]+"/"+gh[1]+"/"+gh[0];
	
	 String[] arrOfStr1 = checkout.split("-",3);
		
	 ArrayList <Integer> b1=new    ArrayList<Integer>();
	 
	 for (String a : arrOfStr1) { 
		  int number=Integer.parseInt(a);
		  b1.add(number);
    
	  } 
	 
 Iterator df1=b1.iterator();
 
 String[] gh1=new String[3];
 int j1=0;
 while(df1.hasNext()) {
	 gh1[j1]= ""+df1.next();
	 j1++;
	 
 }
 
 
 

String fv1=gh1[2]+"/"+gh1[1]+"/"+gh1[0];
	 
      System.out.println(fv);
      System.out.println(fv1);
		try
		 {
		con = DBConnection.createConnection(); 
		 PreparedStatement stmt=con.prepareStatement("insert into insertimage(hostelid,specifications,beds,price,checkin,checkout,guests,rooms) values(?,?,?,?,to_date(?,'dd/mm/yyyy'),to_date(?,'dd/mm/yyyy'),?,?)");
         stmt.setString(1,hostelid);
         stmt.setString(2,specifications);
		 stmt.setString(3,beds);
		 stmt.setString(4,price);
		 stmt.setString(5,fv);
		 stmt.setString(6,fv1);
		 stmt.setString(7,guests);
		 stmt.setString(8,rooms);
		 int i=stmt.executeUpdate();  
			System.out.println(i+" record inserted");  
			con.close();  
		 }
		 catch(SQLException e)
		 {
		 e.printStackTrace();
		 }
		 return "Invalid user credentials"; 
	     }
	public void insertImage(InputStream inputStream, String hostelid) {
		try {
			Connection con = DBConnection.createConnection();
			System.out.println("Hai"); 
			PreparedStatement uploadPic = con.prepareStatement("update insertimage set image=(?) where hostelid = (?)");
			System.out.println("Hai image"); 
			if(inputStream!=null) {
				uploadPic.setBlob(1, inputStream);
				System.out.println("inside If"); 
			}
			uploadPic.setString(2, hostelid);
			uploadPic.executeUpdate();
		}
		catch(Exception imageUploadError) {
			System.out.println(imageUploadError);
			
		}
		
	
}

}

     


