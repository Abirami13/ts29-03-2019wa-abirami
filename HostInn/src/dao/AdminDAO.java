package dao;

import java.sql.*;
import java.sql.SQLException;

import bean.AdminBean;
import connection.DBConnection;
public class AdminDAO {
	public String InsertData(AdminBean insert) {
		String hostelname=insert.getHostelname();
		String roomnumber=insert.getRoomnumber();
		String roomtype =insert.getRoomtype();
		String beds=insert.getBeds();
		String price=insert.getPrice();
		
		System.out.println("------------->"+hostelname);
		Connection con = null;
	 
	 
		 
		try
		 {
		 con = DBConnection.createConnection(); 
		 PreparedStatement stmt=con.prepareStatement("insert into adminentry values(?,?,?,?,?)");
		 stmt.setString(1,hostelname);
	     stmt.setString(2, roomnumber);
		 stmt.setString(3,roomtype);
		 stmt.setString(4,beds);
		 stmt.setString(5,price);
	
		 
		 	
		 int i=stmt.executeUpdate();  
			System.out.println(i+" record inserted");  
			  
			con.close();  
		 }
		 catch(SQLException e)
		 {
		 e.printStackTrace();
		 }
		 return "Invalid user credentials"; 
	}

}


