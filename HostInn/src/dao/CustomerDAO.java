package dao;
import java.sql.*;
import java.sql.SQLException;


import bean.CustomerBean;
import connection.DBConnection;
public class CustomerDAO {
	public String InsertData(CustomerBean insert) {
		String firstname=insert.getFirstname();
		String lastname=insert.getLastname();
		String emailaddress=insert.getEmailaddress();
		
		
		System.out.println("------------->"+firstname);
		Connection con = null;
	 
	 
		 
		try
		 {
		 con = DBConnection.createConnection(); 
		 PreparedStatement stmt=con.prepareStatement("insert into customerdata values(?,?,?)");
		 stmt.setString(1, firstname);
		 stmt.setString(2,lastname);
		 stmt.setString(3,emailaddress);

		 
		 	
		 int i=stmt.executeUpdate();  
			System.out.println(i+" record inserted");  
			  
			con.close();  
		 }
		 catch(SQLException e)
		 {
		 e.printStackTrace();
		 }
		 return "Invalid user credentials"; 
	}

}



