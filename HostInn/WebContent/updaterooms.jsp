<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<body>
<style>
body {font-family: Arial, Helvetica, sans-serif;}

*{
box-sizing:border-box;
}
input[type=text], input[type=password] {
  width: 40%;
  padding: 15px;
  margin: 5px 100px 22px 15%;
  display:block;
  border-radius: 50px;
}
input[type=text]:focus, input[type=password]:focus{
outline:none;
}
button {
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 20%;
  opacity:0.9;
  border-radius: 5px;
}

button:hover {
  opacity: 1;
}

.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: #f44336;
}

.imgcontainer {
  text-align: center;
  margin: 24px 0 12px 0;
}


.container {
  padding: 16px;
}


/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  span.psw {
     display: block;
     float: none;
  }
  .cancelbtn {
     width: 100%;
  }
}
</style>
<script>
function myFunction() {
  alert("UPDATED SUCCESSFULLY");
}
</script>

</head>
<body>
<button type="button" class="backbutton" onClick="window.location.href='adminpage.jsp'">Back</button>

<center>
<img class="myimage" src="logo.PNG" width="150px" height="100px" alt="logo">
<center><h1 style="color:2196f3;">HOST INN</h1></center>
  <div class="container">
   <form action="UpdateServlet"  method="post">
    <label for="uname"><b>ENTER HOSTEL NAME</b></label>
   
    
    <input type="text"  name="hostelname" >

    <label for="ffd"><b>ENTER ROOM NUMBER</b></label>
    
    <input type="text" id="psw" name="ROOM NUMBER" required>
    <label for="uname"><b>ENTER ROOM TYPE</b></label>
    <input type="text"  name="ROOM TYPE" >
    
    <label for="uname"><b>ENTER BEDS</b></label>
    <input type="text"  name="BEDS" >
    
    <label for="uname"><b>ENTER PRICE</b></label>
    <input type="text"  name="PRICE" >
    
    
        
        
    <button type="submit" onClick="myFunction()">UPDATE</button>
    
 </div>

  <div class="container" style="background-color:#f1f1f1">
    <button type="button" class="cancelbtn"
    onClick="window.location.href='index.jsp'">Cancel</button>
	
  </div>
  </center>
</form>

</body>
</html>