<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  font-family: Arial, Helvetica, sans-serif;
  background-color: white;
}

* {
  box-sizing: border-box;
}


.container {
  padding: 16px;
  background-color: white;
}

input[type=text], input[type=password] {
  width: 30%;
  padding: 20px;
  margin: 5px 0 22px 0;
  display: inline-block;
  border: none;
  background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

/* Overwrite default styles of hr */
hr {
  border: 1px solid #f1f1f1;
  margin-bottom: 25px;
}


.registerbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 30%;
  opacity: 0.9;
}
button {
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 20%;
  opacity:0.9;
  border-radius: 5px;
}


.registerbtn:hover {
  opacity: 1;
}


a {
  color: dodgerblue;
}


</style>
<script>
function myFunction() {
  alert("DELETED SUCCESSFULLY");
}
</script>

</head>
<body>
<button type="button" class="backbutton" onClick="window.location.href='adminpage.jsp'">Back</button>

<center><img class="myimage" src="logo.PNG"  width="150px" height="100px" alt="logo"></center>
<center><h1 style="color:black;">HOST INN</h1></center>

<form action="DeleteServlet" method="post">
<center> <div class="container">
    <h1>DELETE ROOM DETAILS</h1>
  

    <label for="ROOM NUMBER"><b>roomnumber</b></label>
    <input type="text"  name="ROOM NUMBER" required><br>

        <button type="submit" onClick="myFunction()">DELETE ROOMS</button>
  </div>
  <button type="button" class="cancelbtn"
    onClick="window.location.href='index.jsp'">Cancel</button>
	
  
 </center>
  
</form>

</body>
</html>
