<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<body>
<style>
body {font-family: Arial, Helvetica, sans-serif;}
form {border: 3px solid #f1f1f1;}

input[type=text], input[type=password] {
  width: 30%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}

.backbutton{
width:75px;
height:50px;
}

button {
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 20%;
}

button:hover {
  opacity: 0.8;
}

.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: #f44336;
}

.imgcontainer {
  text-align: center;
  margin: 24px 0 12px 0;
}


.container {
  padding: 16px;
}


/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  span.psw {
     display: block;
     float: none;
  }
  .cancelbtn {
     width: 100%;
  }
}
</style>
</head>
<script>
function myFunction() {
  alert("REGISTERED SUCCESSFULLY");
}
</script>

<body>
<button type="button" class="backbutton" onClick="window.location.href='index.jsp'">Back</button>


<center>
<img class="myimage" src="logo.PNG" width="150px" height="100px" alt="logo">

<center><h1 style="color:2196f3;">HOST INN</h1></center>
<h2>Registration Form</h2>
  <div class="container">
   <form action="insertdata"  method="post">
    <label for="uname"><b>Username</b></label>
   
    
    <input type="text" placeholder="Enter name" name="Name" required><br>

    <label for="psw"><b>Password</b></label>
     
    <input type="password"  placeholder="enter password" id="psw" size=20 name="psw" required></br> </br>

      <label for="email"><b>Email Address</b></label>
    
    <input type="text"  id="psw" placeholder="enter email address" size=20 name="EMail" required></br> </br>
     
      <label for="phone"><b>Phone Number</b></label>
    
    <input type="text" id="psw"  placeholder="enter phone number" size=20 name="Phonenumber" required></br> </br>
        
        
    <button type="submit" onClick="myFunction()">Register</button><br>
    
 </div>

  <div class="container" style="background-color:#f1f1f1">
    <button type="button" class="cancelbtn"
    onClick="window.location.href='index.jsp'">Cancel</button>
	
  </div>
  </center>
</form>

</body>
</html>